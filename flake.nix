{
  inputs = {
    all-cabal-hashes = {
      url = "github:commercialhaskell/all-cabal-hashes/hackage";
      flake = false;
    };
    nix-vscode-extensions = {
      url = "github:nix-community/nix-vscode-extensions";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    stacklock2nix.url = "github:cdepillabout/stacklock2nix";
  };

  outputs = { self, nix-vscode-extensions, nixpkgs, stacklock2nix, all-cabal-hashes }:
    let
      supportedSystems = [
        "x86_64-linux"
        "x86_64-darwin"
        "aarch64-linux"
        "aarch64-darwin"
      ];
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
      pkgs = system: import nixpkgs {
        inherit system;
        overlays = [
          nix-vscode-extensions.overlays.default
          stacklock2nix.overlay
          self.overlays.default
        ];
      };
    in
    {
      overlays.default = final: prev: {
          transactional-pkgSet = final.stacklock2nix {
            stackYaml = ./stack.yaml;
            stackYamlLock = ./stack.yaml.lock;
            baseHaskellPkgSet = final.haskell.packages.ghc964;
            additionalHaskellPkgSetOverrides = hfinal: hprev: {
              mkDerivation = a: hprev.mkDerivation (a // { doCheck = false; doHaddock = false; });
            };
            additionalDevShellNativeBuildInputs = stacklockHaskellPkgSet: [
              (final.vscode-with-extensions.override rec {
                vscode = final.vscodium;
                vscodeExtensions = with (final.forVSCodeVersion vscode.version).vscode-marketplace; [
                  haskell.haskell
                  jnoortheen.nix-ide
                  justusadam.language-haskell
                  shardulm94.trailing-spaces
                ];
              })
              final.stack
              final.haskell.packages.ghc964.haskell-language-server
              final.haskell.packages.ghc964.fourmolu
            ];
            cabal2nixArgsOverrides = args: args // {
              lmdb-raw = _: {
                lmdb = final.lmdb;
              };
            };
            inherit all-cabal-hashes;
          };
        };
      devShells = forAllSystems (system: {
        default = (pkgs system).transactional-pkgSet.devShell.overrideAttrs (a: {
          shellHook =
            (a.shellHook or "") +
            ''
              export NIX_PATH=nixpkgs=${nixpkgs}
            '';
        });
        stack = (pkgs system).mkShell {
          packages = [ (pkgs system).stack ];
          shellHook =
            ''
              export NIX_PATH=nixpkgs=${nixpkgs}
            '';
        };
      });
      formatter = forAllSystems (system:
        let
          inherit (pkgs system) fd haskell nixpkgs-fmt writeScriptBin;
          inherit (haskell.packages.ghc964) fourmolu;
        in
        writeScriptBin "formatter" ''
          set -ex
          ${fourmolu}/bin/fourmolu -i `${fd}/bin/fd -g *.hs exe lib test`
          ${nixpkgs-fmt}/bin/nixpkgs-fmt `${fd}/bin/fd -g *.nix .`
        '');
      legacyPackages = forAllSystems pkgs;
    };
}
