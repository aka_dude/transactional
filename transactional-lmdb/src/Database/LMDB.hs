module Database.LMDB where

import Control.Monad ((>=>))
import Control.Monad.Catch (MonadMask, bracketOnError)
import Control.Monad.IO.Class (MonadIO (liftIO))
import Data.ByteString (ByteString, packCStringLen, useAsCStringLen)
import Data.Text (Text)
import Data.Text.Encoding (decodeUtf8With)
import Database.LMDB.Raw (MDB_DbFlag (MDB_CREATE), MDB_EnvFlag (MDB_NOLOCK), MDB_dbi', MDB_env, MDB_txn, MDB_val (..), compileWriteFlags, mdb_dbi_open', mdb_del', mdb_env_create, mdb_env_open, mdb_env_set_mapsize, mdb_env_set_maxdbs, mdb_env_set_maxreaders, mdb_get', mdb_put', mdb_txn_abort, mdb_txn_begin, mdb_txn_commit)
import Foreign (castPtr)

type Key = ByteString
type Value = ByteString

data ConnectionConfig = ConnectionConfig
  { maxTables
    , maxReaders
    , mapSize ::
      Int
  , flags :: [MDB_EnvFlag]
  }

defConnectionConfig :: ConnectionConfig
defConnectionConfig = ConnectionConfig {maxTables = 64, maxReaders = 126, mapSize = 10485760, flags = [MDB_NOLOCK]}

openDatabase :: (MonadIO m) => ConnectionConfig -> FilePath -> m MDB_env
openDatabase ConnectionConfig {maxTables, maxReaders, mapSize, flags} f = liftIO do
  e <- mdb_env_create
  mdb_env_set_maxdbs e maxTables
  mdb_env_set_maxreaders e maxReaders
  mdb_env_set_mapsize e mapSize
  mdb_env_open e f flags
  pure e

data Mode = ReadWrite | ReadOnly

isReadOnly :: Mode -> Bool
isReadOnly ReadWrite = False
isReadOnly ReadOnly = True

beginTx :: (MonadIO m) => MDB_env -> Maybe MDB_txn -> Mode -> m MDB_txn
beginTx d t m = liftIO $ mdb_txn_begin d t (isReadOnly m)

commitTx :: (MonadIO m) => MDB_txn -> m ()
commitTx = liftIO . mdb_txn_commit

abortTx :: (MonadIO m) => MDB_txn -> m ()
abortTx = liftIO . mdb_txn_abort

withTx :: (MonadIO m, MonadMask m) => MDB_env -> Mode -> (MDB_txn -> m a) -> m a
withTx db mode f = bracketOnError (beginTx db Nothing mode) abortTx \tx -> f tx <* commitTx tx

{- |
First argument denotes what kind of transaction will be used to open the table.
Additionally, in ReadWrite mode, table will be created if missing (MDB_CREATE flag)
-}
openTableWithMode :: (MonadIO m) => Mode -> MDB_env -> Maybe String -> m MDB_dbi'
openTableWithMode mode db n = liftIO do
  tx <- beginTx db Nothing mode
  r <-
    mdb_dbi_open' tx n case mode of
      ReadWrite -> [MDB_CREATE]
      ReadOnly -> []
  commitTx tx
  pure r

tableGet :: (MonadIO m) => MDB_txn -> MDB_dbi' -> Key -> m (Maybe Value)
tableGet tx t k =
  liftIO $
    byteStringToVal k $
      mdb_get' tx t
        >=> \case
          Nothing -> pure Nothing
          Just v' -> Just <$> valToByteString v'

-- | Returns @False@ if duplication is enabled and duplication has occured
tablePut :: (MonadIO m) => MDB_txn -> MDB_dbi' -> Key -> Maybe Value -> m Bool
tablePut tx t k (Just v) = liftIO $ byteStringToVal k $ byteStringToVal v . mdb_put' (compileWriteFlags []) tx t
tablePut tx t k Nothing = liftIO $ byteStringToVal k \k' -> mdb_del' tx t k' Nothing

-- Utils

byteStringToVal :: ByteString -> (MDB_val -> IO a) -> IO a
byteStringToVal b f = useAsCStringLen b \(p, l) -> f MDB_val {mv_size = toEnum l, mv_data = castPtr p}

valToByteString :: MDB_val -> IO ByteString
valToByteString MDB_val {mv_size, mv_data} = packCStringLen (castPtr mv_data, fromEnum mv_size)

decodeUtf8Lenient :: ByteString -> Text
decodeUtf8Lenient = decodeUtf8With \_err _val -> Just '\xFFFD'
