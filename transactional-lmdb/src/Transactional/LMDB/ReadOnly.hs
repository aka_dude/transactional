module Transactional.LMDB.ReadOnly (DatabaseRO, readOnly, TableRO, openTableRO, tableGetRO) where

import Control.Concurrent.STM (STM, TVar)
import Control.Monad.IO.Class (MonadIO (liftIO))
import Data.Coerce (coerce)
import Data.Data ((:~:) (Refl))
import Data.Strict.Wrapper
import Database.LMDB (Key, Mode (ReadOnly, ReadWrite), Value, abortTx, beginTx, commitTx, decodeUtf8Lenient, openTableWithMode, tableGet)
import Database.LMDB.Raw (MDB_dbi', MDB_env, MDB_txn)
import Effectful (IOE, (:>))
import Transactional (Transactional (..), TxLog, run)
import Transactional.LMDB.ReadWrite (Database (Database))

newtype DatabaseRO = DatabaseRO MDB_env

readOnly :: Database -> DatabaseRO
readOnly = coerce

newtype TableRO = TableRO MDB_dbi'
  deriving stock (Eq)

-- | Open table, creating it if it doesn't exist
openTableRO :: (MonadIO m) => DatabaseRO -> Maybe String -> m TableRO
openTableRO d t = coerce <$> openTableWithMode ReadOnly (coerce d) t

instance Transactional DatabaseRO where
  newtype Transaction DatabaseRO = TransactionRO MDB_txn
  type CtxtE DatabaseRO r = IOE :> r
  begin d = liftIO $ coerce <$> beginTx (coerce d) Nothing ReadWrite
  commit = liftIO . commitTx . coerce
  abort = liftIO . abortTx . coerce
  data ActionRequest DatabaseRO result where
    TableGetRequest :: TableRO -> Key -> ActionRequest DatabaseRO (Maybe Value)

  runRequest (TableGetRequest table key) dbTx = liftIO $ tableGet (coerce dbTx) (coerce table) key

  eqActionRequest :: ActionRequest DatabaseRO a -> ActionRequest DatabaseRO b -> Strict (Maybe (a :~: b))
  TableGetRequest t k `eqActionRequest` TableGetRequest t' k'
    | t == t' && k == k' = Strict $ Just Refl
  _ `eqActionRequest` _ = Strict Nothing

instance Show (ActionRequest DatabaseRO r) where
  show (TableGetRequest _table key) = "TableGetRequest {key = " <> show (decodeUtf8Lenient key) <> "}"

tableGetRO :: TVar (TxLog r DatabaseRO) -> TableRO -> Key -> STM (Maybe Value)
tableGetRO txLog table key = run txLog $! TableGetRequest table key
