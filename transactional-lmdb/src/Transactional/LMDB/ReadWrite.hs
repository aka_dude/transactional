module Transactional.LMDB.ReadWrite (Database (Database), Table (Table), openDatabase, openTable, clearTable, tableGet, tablePut) where

import Control.Concurrent.STM (STM, TVar)
import Control.Monad.IO.Class (MonadIO (liftIO))
import Data.Coerce (coerce)
import Data.Data ((:~:) (Refl))
import Data.Strict.Wrapper
import Database.LMDB (ConnectionConfig, Key, Mode (ReadWrite), Value, abortTx, beginTx, commitTx, decodeUtf8Lenient, openTableWithMode)
import Database.LMDB qualified as DB (openDatabase, tableGet, tablePut)
import Database.LMDB.Raw (MDB_dbi', MDB_env, MDB_txn, mdb_clear')
import Effectful (IOE, (:>))
import Transactional (Transactional (..), TxLog, run)

newtype Database = Database MDB_env

openDatabase :: (MonadIO m) => ConnectionConfig -> FilePath -> m Database
openDatabase conf path = coerce <$> DB.openDatabase conf path

newtype Table = Table MDB_dbi'
  deriving stock (Eq)

-- | Open table, creating it if it doesn't exist
openTable :: (MonadIO m) => Database -> Maybe String -> m Table
openTable d t = coerce <$> openTableWithMode ReadWrite (coerce d) t

-- TODO: implement @mdb_txn_env@ in bindings and get rid of @Database@ argument
clearTable :: (MonadIO m) => Database -> Table -> m ()
clearTable db t = liftIO do
  tx <- beginTx (coerce db) Nothing ReadWrite
  mdb_clear' tx (coerce t)
  commitTx tx

instance Transactional Database where
  newtype Transaction Database = Transaction MDB_txn
  type CtxtE Database r = IOE :> r
  begin d = liftIO $ coerce <$> beginTx (coerce d) Nothing ReadWrite
  commit = liftIO . commitTx . coerce
  abort = liftIO . abortTx . coerce
  data ActionRequest Database result where
    TableGetRequest :: Table -> Key -> ActionRequest Database (Maybe Value)
    TablePutRequest :: Table -> Key -> Maybe Value -> ActionRequest Database Bool

  runRequest (TableGetRequest table key) dbTx = liftIO $ DB.tableGet (coerce dbTx) (coerce table) key
  runRequest (TablePutRequest table key value) dbTx = liftIO $ DB.tablePut (coerce dbTx) (coerce table) key value

  eqActionRequest :: ActionRequest Database a -> ActionRequest Database b -> Strict (Maybe (a :~: b))
  TableGetRequest t k `eqActionRequest` TableGetRequest t' k'
    | t == t' && k == k' = Strict $ Just Refl
  TablePutRequest t k v `eqActionRequest` TablePutRequest t' k' v'
    | t == t' && k == k' && v == v' = Strict $ Just Refl
  _ `eqActionRequest` _ = Strict Nothing

instance Show (ActionRequest Database r) where
  show (TableGetRequest _table key) = "TableGetRequest {key = " <> show (decodeUtf8Lenient key) <> "}"
  show (TablePutRequest _table key value) =
    "TablePutRequest {key = "
      <> show (decodeUtf8Lenient key)
      <> ", value = "
      <> maybe "Nothing" (mappend "Just " . show . decodeUtf8Lenient) value
      <> "}"

tableGet :: TVar (TxLog r Database) -> Table -> Key -> STM (Maybe Value)
tableGet txLog table key = run txLog $! TableGetRequest table key

tablePut :: TVar (TxLog r Database) -> Table -> Key -> Maybe Value -> STM Bool
tablePut txLog table key value = run txLog $! TablePutRequest table key value
