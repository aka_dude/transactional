{-# LANGUAGE TemplateHaskell #-}

module Main where

import Colog (Message, richMessageAction)
import Control.Monad (void)
import Control.Monad.Catch (Exception, catch)
import Database.LMDB (defConnectionConfig)
import Effectful (Eff, IOE, liftIO, runEff)
import Effectful.Colog.Dynamic (Logger, runLogAction)
import Effectful.Concurrent (Concurrent, runConcurrent)
import Effectful.Concurrent.STM (throwSTM)
import GHC.Stack (HasCallStack)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase, (@=?))
import Test.Tasty.TH (defaultMainGenerator)
import Transactional (transactionally)
import Transactional.LMDB (openDatabase, openTable, tableGet, tablePut)
import Utils (spawnClonesAndWait, withTestDatabaseDir)

interpret :: Eff '[Logger Message, Concurrent, IOE] a -> IO a
interpret = runEff . runConcurrent . runLogAction richMessageAction

test_seqReadWrite :: (HasCallStack) => [TestTree]
test_seqReadWrite = pure do
  testCase "Sequential read-write" $ interpret $ withTestDatabaseDir \dir -> do
    db <- openDatabase defConnectionConfig dir
    table <- openTable db Nothing
    spawnClonesAndWait 1000 \value -> do
      let key = "key"
          expected = Just value
      actual <- transactionally db \txl -> do
        void $ tablePut txl table key expected
        tableGet txl table key
      liftIO $ expected @=? actual

data DummyException = DummyException
  deriving stock (Show)

instance Exception DummyException

test_throwRollback :: (HasCallStack) => [TestTree]
test_throwRollback = pure do
  testCase "Exception causes rollback" $ interpret $ withTestDatabaseDir \dir -> do
    db <- openDatabase defConnectionConfig dir
    table <- openTable db Nothing
    let key = "key"
        value = "value"

    ( do
        transactionally db \txl -> do
          void $ tablePut txl table key $ Just value
          throwSTM DummyException
      )
      `catch` const @_ @DummyException (pure ())

    actual <- transactionally db \txl -> do
      tableGet txl table key
    liftIO $ Nothing @=? actual

main :: (HasCallStack) => IO ()
main = $(defaultMainGenerator)
