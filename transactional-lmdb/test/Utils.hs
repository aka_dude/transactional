module Utils where

import Control.Concurrent.STM qualified as Base (newTVarIO)
import Control.Monad.Catch (finally, throwM)
import Data.ByteString (ByteString)
import Data.Foldable (for_)
import Data.Text qualified as Text (pack)
import Data.Text.Encoding (encodeUtf8)
import Data.Traversable (for)
import Effectful (Eff, IOE, liftIO, (:>))
import Effectful.Concurrent (forkFinally)
import Effectful.Concurrent.MVar.Strict (newEmptyMVar, putMVar, takeMVar)
import Effectful.Concurrent.STM (Concurrent, TVar, atomically, readTVar, writeTVar)
import GHC.Stack (HasCallStack)
import System.Directory (createDirectoryIfMissing, getTemporaryDirectory, removeDirectoryRecursive)
import System.FilePath ((</>))
import System.IO.Unsafe (unsafePerformIO)

newTestId :: TVar Integer
newTestId = unsafePerformIO $ Base.newTVarIO 0
{-# NOINLINE newTestId #-}

mkTestId :: (Concurrent :> r) => Eff r Integer
mkTestId = atomically do
  i <- readTVar newTestId
  writeTVar newTestId $ i + 1
  pure i

mkTestDatabaseDir :: (HasCallStack, IOE :> r, Concurrent :> r) => Eff r FilePath
mkTestDatabaseDir = do
  i <- mkTestId
  tmp <- liftIO getTemporaryDirectory
  let dir = tmp </> "transactional" </> ("test-" <> show i)
  liftIO $ createDirectoryIfMissing True dir
  pure dir

withTestDatabaseDir :: (HasCallStack, IOE :> r, Concurrent :> r) => (FilePath -> Eff r a) -> Eff r a
withTestDatabaseDir f = do
  d <- mkTestDatabaseDir
  f d `finally` liftIO (removeDirectoryRecursive d)

spawnClonesAndWait :: (HasCallStack, Concurrent :> r) => Int -> (ByteString -> Eff r ()) -> Eff r ()
spawnClonesAndWait n single = do
  threads <- for [1 .. n] (fork . single . encodeUtf8 . Text.pack . show @Int)
  for_ threads takeMVar
  where
    fork a = do
      v <- newEmptyMVar
      _tid <- forkFinally a \e -> do
        putMVar v ()
        either throwM pure e
      pure v
