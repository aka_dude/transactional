module Transactional (Transactional (..), TxLog, retryAndRerun, run, transactionally) where

import Prelude hiding (log)

import Colog.Core (Severity (Debug))
import Colog.Message (Message)
import Control.Exception (Exception, SomeException)
import Control.Monad.Catch (Handler (Handler), catches, throwM)
import Data.Data (Typeable, (:~:) (Refl))
import Data.Kind (Constraint, Type)
import Data.Strict.Wrapper
import Data.Text qualified as Text (pack)
import Data.Traversable (for)
import Data.Vector (Vector, (!?))
import Data.Vector qualified as V (snoc, take)
import Effectful (Eff, Effect, (:>))
import Effectful.Colog.Dynamic (Logger, log)
import Effectful.Concurrent.STM (Concurrent, STM, TVar, atomically, newTVarIO, readTVar, readTVarIO, throwSTM, writeTVar)
import GHC.Stack (HasCallStack)

class (Typeable d) => Transactional d where
  data Transaction d :: Type
  type CtxtE d (r :: [Effect]) :: Constraint
  begin :: (CtxtE d r) => d -> Eff r (Transaction d)
  commit, abort :: (CtxtE d r) => Transaction d -> Eff r ()
  data ActionRequest d result :: Type
  eqActionRequest :: ActionRequest d a -> ActionRequest d b -> Strict (Maybe (a :~: b))
  runRequest :: (CtxtE d r) => ActionRequest d result -> Transaction d -> Eff r result

data ActionOrRerunRequest d
  = forall r. (Typeable r, Show (ActionRequest d r)) => ActionRequest (ActionRequest d r)
  | RerunRequest {firstInvalidActionIdx :: Int}

instance Show (ActionOrRerunRequest d) where
  show (ActionRequest a) = "ActionRequest (" <> show a <> ")"
  show RerunRequest {firstInvalidActionIdx} = "RerunRequest {firstInvalidActionIdx = " <> show firstInvalidActionIdx <> "}"

instance (Typeable d) => Exception (ActionOrRerunRequest d)

data Action d = forall r. (Typeable r) => Action {request :: ActionRequest d r, response :: r}

data TxLog region d = TxLog
  { actions :: Vector (Action d)
  , currentActionIdx :: Int
  -- ^ an index into `actions`
  }

run :: forall d r region. (Transactional d, Typeable r, Show (ActionRequest d r)) => TVar (TxLog region d) -> ActionRequest d r -> STM r
run vTxLog act = do
  txLog <- readTVar vTxLog
  case txLog.actions !? txLog.currentActionIdx of
    Just Action {request, response} -> case request `eqActionRequest` act of
      -- Current action in log matches requested one
      -- We increment index in log and proceed
      Strict (Just Refl) -> do
        writeTVar vTxLog txLog {currentActionIdx = txLog.currentActionIdx + 1}
        pure response
      -- Current action does not match requested one
      -- Rerun is needed
      Strict Nothing -> throwSTM (RerunRequest {firstInvalidActionIdx = txLog.currentActionIdx} :: ActionOrRerunRequest d)
    -- Requested action has not been run yet
    Nothing -> throwSTM $ ActionRequest act

retryAndRerun :: forall d a region. (Transactional d) => TVar (TxLog region d) -> STM a
retryAndRerun _ = throwSTM (RerunRequest {firstInvalidActionIdx = 0} :: ActionOrRerunRequest d)

writeTVarIO :: (Concurrent :> r) => TVar a -> a -> Eff r ()
writeTVarIO v = atomically . writeTVar v

modifyTVar :: TVar a -> (a -> a) -> STM ()
modifyTVar v f = readTVar v >>= writeTVar v . f

modifyTVarIO :: (Concurrent :> r) => TVar a -> (a -> a) -> Eff r ()
modifyTVarIO v = atomically . modifyTVar v

transactionally :: forall d r a. (HasCallStack, Transactional d, CtxtE d r, Concurrent :> r, Logger Message :> r) => d -> (forall {region :: Type}. TVar (TxLog region d) -> STM a) -> Eff r a
transactionally db f = do
  vDbTx <- newTVarIO $ Nothing @(Transaction d)
  vTxLog <- newTVarIO TxLog {actions = mempty, currentActionIdx = 0}
  txRunner vDbTx vTxLog $ atomically $ f vTxLog
  where
    txRunner :: (HasCallStack) => TVar (Maybe (Transaction d)) -> TVar (TxLog () d) -> Eff r a -> Eff r a
    txRunner vDbTx vTxLog stmTx =
      ( do
          r <- stmTx
          maybe (pure ()) commit =<< readTVarIO vDbTx
          pure r
      )
        `catches` [ Handler $ onRequest' vDbTx vTxLog stmTx
                  , Handler \(e :: SomeException) -> do
                      log Debug "Got an exception"
                      readTVarIO vDbTx >>= \case
                        Nothing -> log Debug "dbTx is empty"
                        Just dbTx -> do
                          abort dbTx
                          log Debug "Aborted dbTx"
                      throwM e
                  ]

    onRequest' :: (HasCallStack) => TVar (Maybe (Transaction d)) -> TVar (TxLog () d) -> Eff r a -> ActionOrRerunRequest d -> Eff r a
    onRequest' vDbTx vTxLog stmTx r = do
      log Debug $ "received " <> Text.pack (show r)
      onRequest vDbTx vTxLog stmTx r

    onRequest :: (HasCallStack) => TVar (Maybe (Transaction d)) -> TVar (TxLog () d) -> Eff r a -> ActionOrRerunRequest d -> Eff r a
    onRequest vDbTx vTxLog stmTx = \case
      RerunRequest {firstInvalidActionIdx = 0} -> do
        readTVarIO vDbTx >>= \case
          Nothing -> pure ()
          Just dbTx -> do
            abort dbTx
            writeTVarIO vDbTx Nothing
        modifyTVarIO vTxLog \txLog -> txLog {actions = mempty}
        txRunner vDbTx vTxLog stmTx
      RerunRequest {firstInvalidActionIdx} -> do
        readTVarIO vDbTx >>= maybe (pure ()) abort
        dbTx <- begin db
        writeTVarIO vDbTx $ Just dbTx
        TxLog {actions = prevActions} <- readTVarIO vTxLog
        newActions <- for
          (V.take firstInvalidActionIdx prevActions)
          \Action {request} -> Action request <$> runRequest request dbTx
        modifyTVarIO vTxLog \txLog -> txLog {actions = newActions}
        txRunner vDbTx vTxLog stmTx
      ActionRequest request -> do
        dbTx <-
          readTVarIO vDbTx >>= \case
            Just d -> pure d
            Nothing -> do
              d <- begin db
              writeTVarIO vDbTx $ Just d
              pure d
        response <- runRequest request dbTx
        modifyTVarIO vTxLog \txLog -> txLog {actions = txLog.actions `V.snoc` Action {request, response}}
        txRunner vDbTx vTxLog stmTx
